# Fantasy Fonts
Fonts for fantasy texts, currently comprised of three families. Simply set any text to use a font and it will be transformed. They can be thought of as simple ciphers where each letter is displayed using a symbol. Being English-centric there is good ASCII coverage, but there is (depending on the font) some additional European coverage (e.g., accented vowels, thorn, edth, etc.).

None of these fonts are finished so there may be some spacing issues and other problems, but they are well suited for personal use. In the spirit of open source they are all distributed with the SIL Open Font license though the font development files (Glyph) are not (nor are they included).

## Realm Elvish ![image](RealmElvish.png)
These fonts are inspired by a script used in 'Wizards Realm' role-playing game. If advanced OpenType features are available then certain substitutions, such as 's' + 'h' -> 'sh', will be done. As in my own work there is a tendency to use Roman names for elves the numbers are represented with roman numerals. This is accomplished through OpenType features so numbers are entered normally and the font handles their display. Thus you enter '10' and the font displays 'X'. Delete the zero and it will display 'I'.

## Wizardic ![image](Wizardic.png)
These fonts adapt the letter forms of Realm Elvish to a Sanskrit theme. If your software has good OpenType support then you can enable advanced features where each letter has an initial, final and independent form in addition to the base medial form. Further features represent vowels as marks over preceeding consonants. To keep with the exotic flavor the numbers are styled after the Hindi-derived numerals in use in the Middle East.

![image](Wizardic.gif)

## Railog ![image](Paryst.png)
These fonts render Railog scripts with a focus on the 'universal' script used by most Gen-o-Lar languages and named for the trade language. Although these are intended for actual languages, as those languages are fictional for convenience they make use of the standard roman letter code points and as a result match fairly well with ASCII. As the Gen-o-Lar number system is a non-decimal octal there are digits 1 through 8, but there is no 9. Although there is a glyph for zero this is not a decimal system OpenType features are used to give proper representation. That is, numeric values should be entered as standard octal and they will be rendered with the correct glyphs. So 10 becomes 8, 20 becomes 18, and so on.
